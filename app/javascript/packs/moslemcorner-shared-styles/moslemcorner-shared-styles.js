import { PolymerElement, html } from '@polymer/polymer';

export const mcsharedStyles = html`
        <style>
        :root {
            --primary-color: #4285f4;
        }
        [hidden] {
            display: none !important;
        }
        .card {
            margin: var(--card-margin, 24px);
            padding: 20px;
            color: #757575;
            border-radius: 5px;
            background-color: #fff;
            box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
        }
        .alert-error {
            background: #ffcdd2;
            border: 1px solid #f44336;
            border-radius: 3px;
            color: #333;
            font-size: 14px;
            padding: 10px;
        }
        </style>
        `;
