import { PolymerElement, html } from '@polymer/polymer';
import {mixinBehaviors} from '@polymer/polymer/lib/legacy/class';

import { mcsharedStyles} from "../moslemcorner-shared-styles/moslemcorner-shared-styles";
import {DataGridTemplatizerBehavior} from "./moslemcorner-data-grid-templatizer-behavior";
console.log(DataGridTemplatizerBehavior);
class MCDataGridCell extends mixinBehaviors([DataGridTemplatizerBehavior], PolymerElement) {
    static get template(){
        return html`
        ${mcsharedStyles}
        <style >
            :host {
                display: inline-block;
                max-width: var(--data-grid-cell-max-width, 20%);
                width: var(--data-grid-cell-width, 20%);
            }
        </style>
        <content></content>
    `;
    }
    static get properties() {
        return {
                    header: Boolean,
                    column: Object,
                    width: String,
                    align: String
        }
    }
    static get observers(){
        return  [
        '_columnChanged(_instance, column)',
        '_widthChanged(width)',
        '_alignChanged(align)'
        ]
    }
    attached() {
        if (!Polymer.Settings.useNativeShadow) {
            // cell is supposed to be placed outside the local dom of iron-data-table.
            Polymer.StyleTransformer.dom(this, 'data-grid', this._scopeCssViaAttr, true);
            if (this.domHost) {
                Polymer.StyleTransformer.dom(this, this.domHost.tagName.toLowerCase(), this._scopeCssViaAttr, false);
            }
        }
    }
    _columnChanged(_instance, column) {
        _instance.column = column;
        }
    _widthChanged(width) {
            this.style.width = width;
    }
    _alignChanged(align) {
            this.style.textAlign = align;
    }
        
}
customElements.define('moslemcorner-data-grid-cell', MCDataGridCell);
//behaviors: [ moslemcorners.DataGridTemplatizerBehavior ],
                