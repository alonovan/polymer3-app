require 'fileutils'

Dir['vendor/assets/components/**/', 'vendor/assets/components/**/.*'].each do |x|
    puts "processing #{x}"
    if  (x.include? 'demo') || (x.include? 'test') || (x.include? '.github') || (x.include? 'guides') ||
        (x.include? 'man') || (x.include? 'bin') || (x.include? 'templates') || (x.include? 'site')

        begin
            FileUtils.rm_r x
            puts "=== DELETING FOLDER #{x} ==="
        rescue
            puts "nothing todo for #{x} already deleted"
        end
    else
        files = Dir.glob(File.join(x, '*'), File::FNM_DOTMATCH)
        files.each do |file|
            if  (file.include? '.json') || (file.include? 'hero.svg') ||
                (file.include? '.md') || (file.include? '.yml') ||
                (file.include? 'index.html') || (file.include? 'LICENSE') ||
                (file.include? '.gitignore') || (file.include? 'COPYING') ||
                (file.include? 'Gruntfile.js') || (file.include? 'gulpfile.js') ||
                (file.include? 'Gulpfile.js') || (file.include? '.log') ||
                (file.include? '.editorconfig') || (file.include? '.gitattributes') ||
                (file.include? '.npmignore') || (file.include? '.sh') ||
                (file.include? 'Makefile')

                FileUtils.rm_r file
                puts "=== DELETING FILE #{file} ==="
            end
        end
    end
end


# -------------------------------------------------------------------------------------------------------------------------
# node => 9.11.1, npm => 5.6.0, yarn => 1.6.0
# -------------------------------------------------------------------------------------------------------------------------
# Cara add @polymer dan component-component yang berhubungan dengannya
# -------------------------------------------------------------------------------------------------------------------------
# yarn add [module-name] -D
# contoh: yarn add @polymer/polymer -D
# -------------------------------------------------------------------------------------------------------------------------
# Agar module hanya disave sebagai devDependency
# -------------------------------------------------------------------------------------------------------------------------

# -------------------------------------------------------------------------------------------------------------------------
# Script ini berguna untuk memindahkan module ke app/javascript/
# agar module dapat di bundle oleh webpack
# -------------------------------------------------------------------------------------------------------------------------

target_path = 'app/javascript/'

# -------------------------------------------------------------------------------------------------------------------------
# whitelisted folder
# -------------------------------------------------------------------------------------------------------------------------
wl = /^node_modules\/@polymer|@webcomponents|@vaadin/i

# -------------------------------------------------------------------------------------------------------------------------
# copy modules
# -------------------------------------------------------------------------------------------------------------------------
items = Dir["node_modules/*"]
items.each do |item|
    if wl.match(item)
        puts ">>> copying #{item} to #{target_path} .."
        FileUtils.cp_r item, target_path
        FileUtils.rm_rf item
    end
end

# -------------------------------------------------------------------------------------------------------------------------
# blacklisted folder
# -------------------------------------------------------------------------------------------------------------------------
bl = /^.*\/(test|tests|demo|node_modules)/i

# -------------------------------------------------------------------------------------------------------------------------
# remove unnecessary files, to prevent errors when webpack compiling
# -------------------------------------------------------------------------------------------------------------------------
# items2 = Dir["app/javascript/*/*/*"]
# items2.each do |item2|
#     if bl.match(item2)
#         puts "removing #{item2}.."
#         FileUtils.rm_rf item2
#     end
# end
