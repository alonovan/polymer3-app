class Lumo extends HTMLElement {
  static get version() {
    return '1.2.0';
  }
}

customElements.define('vaadin-lumo-styles', Lumo);

export { Lumo };
