import { PolymerElement, html } from '@polymer/polymer';
import '@polymer/iron-input/iron-input';
import '@polymer/paper-input/paper-input';
import '@polymer/paper-input/paper-input-container';
import {sharedStyles} from '../shared-styles/shared-styles';


class UserForm extends PolymerElement {
    static get properties() {
        return {
            actionUrl: {
                type:String,
                value:''
            } ,
            core_user: {
                type: Object,
                value: {}
            },
            formAuthenticityToken: {
                type:String,
                value:''
            },
            title: {
                type: String,
                value:''
            },
            icon:{
                type:String,
                value:''
            },
            _mode:{
                type:String,
                value:'new'
            } ,
            _error:{
                type:String,
                value:''
            },
        }
    }    
    static get template() {
        return html`
        ${sharedStyles}
        <style >
            :host {
                display: block;
                margin-top: 10px;
            }
            .wrapper-btns {
                margin-top: 15px;
                text-align: right;
            }
            paper-button {
                margin-top: 10px;
            }
            paper-button.indigo {
                background-color: var(--paper-indigo-500);
                color: white;
                --paper-button-raised-keyboard-focus: {
                    background-color: var(--paper-pink-a200) !important;
                    color: white !important;
                };
            }
            paper-button.green {
                background-color: var(--paper-green-500);
                color: white;
            }
            paper-button.green[active] {
                background-color: var(--paper-red-500);
            }
            paper-progress {
                width: 100%;
            }
            #formContainer {
                width: var(--user-form-width, 100%);
                margin: 0 auto;
                @apply(--user-form);
            }
            .title {
                margin-bottom: 10px;
            }
            .title > div {
                display: flex;
                flex-direction: row;
                justify-content: flex-start;
                align-items: center;

                padding: 5px 0 5px 0;
                border-bottom: 2px solid #757575;
                font-size: 16px;
                font-weight: bold;
            }
            .title iron-icon {
                padding: 0;
                padding-right: 2px;
            }
        </style>

        <iron-ajax
                id="saveAjax"
                method="post"
                url="[[actionUrl]]"
                content-type="application/json"
                handle-as="json"
                on-response="_onSaveResponse"
                on-error="_onSaveError">
        </iron-ajax>
        <iron-ajax
                id="updateAjax"
                method="put"
                content-type="application/json"
                handle-as="json"
                on-response="_onUpdateResponse"
                on-error="_onUpdateError">
        </iron-ajax>
        <iron-ajax
                id="editAjax"
                method="get"
                content-type="application/json"
                handle-as="json"
                on-response="_onEditResponse"
                on-error="_onEditError">
        </iron-ajax>

        <div class="title">
            <div><iron-icon icon="[[icon]]"></iron-icon>[[title]]</div>
            <paper-progress id="progress" hidden indeterminate></paper-progress>
        </div>

        <div id="formContainer">
            <template is="dom-if" if="[[_error]]">
                <p class="alert-error">[[_error]]</p>
            </template>

            <input is="iron-input" id="id" type="hidden" value="{{core_user.id}}">

            <paper-input label="Email" type="text" id="email" value="{{core_user.email}}"></paper-input>
            <paper-input label="Username" type="text" id="username" value="{{core_user.username}}"></paper-input>
            <paper-input label="Password" type="password" id="password" value="{{core_user.password}}"></paper-input>
            <paper-input label="Confirmation Password" type="password" id="confirmationPassword" value="{{core_user.confirmation_password}}"></paper-input>
            <paper-input label="First Name" type="text" id="firstname" value="{{core_user.firstname}}"></paper-input>
            <paper-input label="Last Name" type="text" id="lastname" value="{{core_user.lastname}}"></paper-input>
            <div class="wrapper-btns">
                <paper-button class="link" on-tap="_cancel" dialog-dismiss>Cancel</paper-button>
                <paper-button raised class="indigo" on-tap="_save">Save</paper-button>
            </div>
        </div>
        `;
    }
    edit(id) {
        this.$.editAjax.url = this.actionUrl +'/'+ id +'/edit';
        this.$.editAjax.generateRequest();
        this._mode = 'edit';
    }
    copy(id) {
        this.$.editAjax.url = this.actionUrl +'/'+ id +'/edit';
        this.$.editAjax.generateRequest();
        this._mode = 'copy';
    }
    _onSaveResponse(e) {
        var response = e.detail.response;
        if (response.status == '200') {
            this._error = '';
            this._mode = 'new';
            this.core_user = {};            
            this.dispatchEvent(new CustomEvent('saveSuccess', {user:response }));
        } else {
            this._error = 'Creating User Error';
        }
        this.$.progress.hidden = true;
    }
    _onSaveError() {
        this._error = 'Creating User Error';
        this.$.progress.hidden = true;
    }
    _onUpdateResponse(e) {
        var response = e.detail.response;
        if (response.status == '200') {
            this._error = '';
            this._mode = 'new';
            this.core_user = {};
            this.dispatchEvent(new CustomEvent('saveSuccess', {user:response }));
        }
        else {
            this._error = 'Updating User Error';
        }
        this.$.progress.hidden = true;
    }
    _onUpdateError() {
        this._error = 'Creating User Error';
        this.$.progress.hidden = true;
    }
    _onEditResponse(e) {
        var response = e.detail.response;
        this.core_user = response.payload;
        if (this._mode === 'copy') {
            this.core_user.id = ''; // nullify id, we will save it as new document
        }
        this.dispatchEvent(new CustomEvent('editSuccess', {user:this.core_user }));

    }
    _onEditError() {
        this._error = 'Edit User Error';
    }
    _save() {
        console.log(this.core_user,this._mode);
        if (this._mode === 'new' || this._mode === 'copy') {
            this.$.saveAjax.headers['X-CSRF-Token'] = this.formAuthenticityToken;
            this.$.saveAjax.body = this.core_user;
            this.$.saveAjax.generateRequest();
            this.$.progress.hidden = false;
        }
        else {
            this.$.updateAjax.headers['X-CSRF-Token'] = this.formAuthenticityToken;
            this.$.updateAjax.body = this.core_user;
            this.$.updateAjax.url = this.actionUrl +'/'+ this.core_user.id;
            this.$.updateAjax.generateRequest();
            this.$.progress.hidden = false;
        }
    }
    _cancel() {
        this._error = '';
        this._mode = 'new';
        this.core_user = {};        
        this.dispatchEvent(new CustomEvent('cancel', {user:{} }));
    }

}
customElements.define('user-form', UserForm);
