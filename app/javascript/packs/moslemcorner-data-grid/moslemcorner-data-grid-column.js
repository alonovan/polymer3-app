import { PolymerElement, html } from '@polymer/polymer';

import {dom} from '@polymer/polymer/lib/legacy/polymer.dom';
class MCDataGridColumn extends PolymerElement {
    static get template(){
        return html`
        <template id="header">
            <div>[[column.name]]</div>
        </template>
        `;
    }
    static get properties() {
        return {
                    /**
                     * Name of the column. This value is displayed in the header cell of the column
                     */
                    name: {
                        type: String,
                        value: ''
                    },
                    /**
                     * Minimum width of the column
                     */
                    width: {
                        type: String,
                        value: "100px"
                    },
                    /**
                     * Text Align
                     */
                    align: {
                        type: String,
                        value: "left"
                    },
                    /**
                     * Template for the header cell
                     */
                    headerTemplate: {
                        type: Object,
                        readOnly: true,
                        value: function () {
                            // Every Polymer element has a this.root property which is the root of its local DOM tree.
                            // https://www.polymer-project.org/1.0/docs/devguide/local-dom#light-dom-children
                            return dom(this.root).querySelector('#header');
                        }
                    },
                    /**
                     * Template for the row item cell
                     */
                    template: {
                        type: Object,
                        readOnly: true,
                        value: function() {
                            // Work with light DOM children
                            // https://www.polymer-project.org/1.0/docs/devguide/local-dom#light-dom-children
                            var template = dom(this).querySelector('template:not([is=header])');
                            if (template) {
                                if (this.dataHost) {
                                    // set dataHost to the context where template has been defined
                                    template._rootDataHost = this.dataHost._rootDataHost || this.dataHost;
                                }
                                return template;
                            }
                        }
                    }
                }
            
    }
}
customElements.define('moslemcorner-data-grid-column', MCDataGridColumn);
