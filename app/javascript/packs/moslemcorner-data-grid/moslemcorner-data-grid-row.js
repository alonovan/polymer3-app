import { PolymerElement, html } from '@polymer/polymer';

import {mcsharedStyles} from "../moslemcorner-shared-styles/moslemcorner-shared-styles";

class MCDataGridRow extends PolymerElement{
    static get template(){
        return html`
        ${mcsharedStyles}
        <style >
            :host {
                display: block;
                max-width: var(--data-grid-cell-max-width, 100%);
                width: var(--data-grid-cell-width, 100%);
            }
        </style>
        <content select="moslemcorner-data-grid-cell"></content>
    `;
    }
}
customElements.define('moslemcorner-data-grid-row', MCDataGridRow);
