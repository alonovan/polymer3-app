import { PolymerElement, html } from '@polymer/polymer';
import '@polymer/iron-ajax/iron-ajax';
import '@polymer/iron-list/iron-list';
import '@polymer/iron-scroll-threshold/iron-scroll-threshold';
import '@polymer/iron-icon/iron-icon';
import '@polymer/iron-icons/iron-icons';
import '@polymer/paper-dialog/paper-dialog';
import '@polymer/paper-button/paper-button';
import '@polymer/paper-icon-button/paper-icon-button';
import '@polymer/paper-fab/paper-fab';
import '@polymer/paper-spinner/paper-spinner';

import '@vaadin/vaadin-grid/vaadin-grid';
import '@vaadin/vaadin-grid/vaadin-grid-sorter';


import '../user-form/user-form';
import {sharedStyles} from '../shared-styles/shared-styles';
import '../moslemcorner-data-grid/moslemcorner-data-grid';
import '../moslemcorner-data-grid/moslemcorner-data-grid-column';


class UserList extends PolymerElement {
    static get properties() {
        return {
            formAuthenticityToken: {
                type:String,
                value:''
            },
            dataUrl: {
                    type: String,
                    value: ''
            },
            _id: {
                type: String,
                value: ''
            },
            users : {
                type: Array,
                value:[]
            }

        }
    }
    static get template() {
        return html`
        ${sharedStyles}
        <style>
            :host {
                display: block;
            }
            moslemcorner-data-grid {
                --card-margin: 5px 24px 24px 24px;
            }
            iron-icon {
                padding-left: 10px;
                padding-right: 10px;
            }
            iron-icon:hover {
                cursor: pointer;
            }
            paper-fab {
                --paper-fab-background: var(--paper-indigo-500);
                position: absolute;
                right: 30px;
                bottom: -30px;
            }
            nebula-loader {
                --nebula-loader-dialog-background-color: #2E3032;
                --nebula-loader-dialog-border-color: #FFFFFF;
                --nebula-loader-dialog-color: #ff4081;
            }
            #form {
                width: 50%;
            }
        </style>
        <iron-ajax
                auto
                id="dataAjax"
                url="[[dataUrl]]"
                handle-as="json"
                loading='{{_loading}}'
                on-response='_reload'
                on-error='_onError'>
        </iron-ajax>
        <iron-ajax
                id="deleteAjax"
                handle-as="json"
                loading='{{_loading}}'
                on-response='_onDeleteResponse'
                on-error='_onDeleteError'>
        </iron-ajax>

        <!--iron-ajax auto url="[[dataUrl]]" handle-as="json" last-response="{{users}}"></iron-ajax-->
        <vaadin-grid id="dataGrid" theme="row-dividers" items="[[users]]" column-reordering-allowed multi-sort>
            <vaadin-grid-column width="9em">
                <template class="header">
                <vaadin-grid-sorter path="email">eMail</vaadin-grid-sorter>
                </template>
                <template>[[item.email]]</template>
            </vaadin-grid-column>     
            <vaadin-grid-column width="9em">
                <template class="header">
                <vaadin-grid-sorter path="username">User Name</vaadin-grid-sorter>
                </template>
                <template>[[item.username]]</template>
            </vaadin-grid-column>     
            <vaadin-grid-column width="9em">
                <template class="header">
                <vaadin-grid-sorter path="firstname">First Name</vaadin-grid-sorter>
                </template>
                <template>[[item.firstname]]</template>
            </vaadin-grid-column>     
            <vaadin-grid-column width="9em">
                <template class="header">
                <vaadin-grid-sorter path="lastname">Last Name</vaadin-grid-sorter>
                </template>
                <template>[[item.lastname]]</template>
            </vaadin-grid-column>
            <vaadin-grid-column width="100%">
                <template class="header">
                </template>
                <template>
                    <iron-icon icon="icons:content-copy"   on-tap="_copy" id="[[item.id]]"></iron-icon>
                    <iron-icon icon="icons:delete" on-tap="_confirmation" id="[[item.id]]"></iron-icon>
                    <iron-icon icon="icons:create" on-tap="_edit" id="[[item.id]]"></iron-icon>
                </template>
            </vaadin-grid-column>     
        </vaadin-grid>        
<!--
        <moslemcorner-data-grid data-url="[[dataUrl]]" id="dataGrid">
            <moslemcorner-data-grid-column name="Email" width="20%">
                <template>[[item.email]]</template>
            </moslemcorner-data-grid-column>
            <moslemcorner-data-grid-column name="Username" width="20%">
                <template>[[item.username]]</template>
            </moslemcorner-data-grid-column>
            <moslemcorner-data-grid-column name="First Name" width="30%">
                <template>[[item.firstname]]</template>
            </moslemcorner-data-grid-column>
            <moslemcorner-data-grid-column name="Last Name" width="30%">
                <template>[[item.lastname]]</template>
            </moslemcorner-data-grid-column>
            <moslemcorner-data-grid-column width="100%">
                <template>
                    <iron-icon icon="icons:create" on-tap="_edit" id="[[item.id]]"></iron-icon>
                    <iron-icon icon="icons:delete" on-tap="_confirmation" id="[[item.id]]"></iron-icon>
                    <iron-icon icon="icons:content-copy" on-tap="_copy" id="[[item.id]]"></iron-icon>
                </template>
            </moslemcorner-data-grid-column>
        </moslemcorner-data-grid>
-->
        <paper-dialog class="card" id="form" modal on-iron-overlay-opened="patchOverlay">
            <user-form action-url="[[dataUrl]]" form-authenticity-token="[[formAuthenticityToken]]" id="userForm"></user-form>
        </paper-dialog>
        <paper-dialog class="card" id="confirmation" modal on-iron-overlay-opened="patchOverlay">
            <div class="title"><iron-icon icon="icons:delete"></iron-icon>Delete Data?</div>
            <div class="buttons">
                <paper-button dialog-dismiss dialog-dismiss>Cancel</paper-button>
                <paper-button on-tap="_delete" dialog-confirm autofocus>Ok</paper-button>
            </div>
        </paper-dialog>
        <paper-fab id="adduser" icon="icons:add" on-tap="_new"></paper-fab>
        <!--paper-spinner id="loader" icon="icons:refresh" text="Please wait..."></paper-spinner-->
    `;
    }    
    ready(){
        super.ready();
        var form = this.$.userForm;
        form.addEventListener('saveSuccess', this._onSaveSuccess.bind(this));
        form.addEventListener('editSuccess', this._onEditSuccess.bind(this));
        form.addEventListener('cancel', this._onCancel.bind(this));
    }

    _new(){
        this.$.userForm.icon = 'icons:add';
        this.$.userForm.title = 'Create New User';
        //this.$.loader.close();
        this.$.form.open();        
    }
    _edit(e){
        console.log('edit',e);
        this.$.userForm.icon = 'icons:create';
        this.$.userForm.title = 'Edit User';
        this.$.userForm.edit(e.target.id);
        //this.$.loader.show();
        this.$.form.open();        
    }
    _delete(e){
        console.log(e,this);
        this.$.deleteAjax.url =  this.dataUrl + '/' + this._id + '/delete';
        this.$.deleteAjax.generateRequest();
    }
    _copy(e){
        console.log('copy',e);
        this.$.userForm.icon = 'icons:content-copy';
        this.$.userForm.title = 'Copy User';
        this.$.userForm.copy(e.target.id);
        //this.$.loader.show();        
    }
    _confirmation(e){
        this._id = e.target.id;
        this.$.confirmation.open();
    }
    _onEditSuccess(e){
        //this.$.dataAjax.generateRequest();
        //this.$.loader.close();
        this.$.form.open();
    }
    _onSaveSuccess(e){
        console.log('savesuccess',e);
        this.$.form.close();
        this.$.dataAjax.generateRequest();
    }
    _onCancel(e){
        this.$.form.close();                    
    }
    _onDeleteResponse(e,response){
        console.log(response)
        if(true){
            this.$.dataAjax.generateRequest();
        }{
            this.$.dataAjax.generateRequest();
        }
        
    }
    _reload(e,resp){
        var self = this;
        console.log(resp.response,this);
        this.users = resp.response.results;
        this.$.dataGrid.clearCache();        
    }
    _openSearch(e){
        console.log(e);
    }
    patchOverlay (e) {
        const overlay = document.querySelector('iron-overlay-backdrop');
        if (e.target.withBackdrop) {
          e.target.parentNode.insertBefore(overlay, e.target);
        }
    }


}
customElements.define('user-list', UserList);
