import '@polymer/polymer/polymer-legacy.js';

import {dom} from '@polymer/polymer/lib/legacy/polymer.dom';
import {PolymerTemplatizer} from '@polymer/polymer/lib/legacy/templatizer-behavior';

let DataGridTemplatizerBehaviorMeta = null;

export const DataGridTemplatizerBehavior = {
    properties: {
        
            item: Object,
            template: Object,
            _instances: { // singleton
                type: Array,
                value: []
            },
            _forwardedParentProps: { // singleton
                type: Object,
                value: {}
            },
            _instance: { // template instance
                type: Object,
                computed: '_templatize(template)'
            }
        
    },
    observers:[ '_itemChanged(_instance, item)' ],  
    created:function() {
            /*
             * In order to determine which properties are instance-specific and require custom forwarding
             * via _forwardInstanceProp/_forwardInstancePath, define an _instanceProps map containing keys for each instance prop
             * https://www.polymer-project.org/1.0/docs/api/Polymer.Templatizer
             */
            this._instanceProps = {
                column: true,
                item: true
            };
    },
        /** templatizer */
    _templatize:function(template) {
        this.templatize(template);
        // fix _rootDataHost to the context where template has been defined
        if (template._rootDataHost) {
            this._getRootDataHost = function() { return template._rootDataHost; };
        }
        var instance = this.stamp({});
        // initializing new template instance with previously forwarded parent props.
        // could be done with observers, but this is simpler.
        for (var key in this._forwardedParentProps) {
            instance[key] = this._forwardedParentProps[key];
        }
        this._instances.push(instance);
        dom(this).insertBefore(instance.root, dom(this).firstElementChild);
        return instance;
    },
    /** templatizer */
    _forwardParentProp:function (prop, value) {
        // store props to initialize new instances.
        this._forwardedParentProps[prop] = value;

        // TODO: Bug in Polymer
        // Seems to two-way bind any parent property only to the last template instance created.
        // We need to push the property to all instances manually.
        this._instances.forEach(function(_instance) {
            _instance[prop] = value;
        });
    },
    _forwardInstanceProp:function(_instance, prop, value) {
        // NOP
    },
    _itemChanged:function(_instance, item) {
        _instance.item = item;
    }
}
