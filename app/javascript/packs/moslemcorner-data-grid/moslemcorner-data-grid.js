import { PolymerElement, html } from '@polymer/polymer';
import {mixinBehaviors} from '@polymer/polymer/lib/legacy/class.js';
import {IronResizableBehavior} from '@polymer/iron-resizable-behavior/iron-resizable-behavior.js';
import * as async from '@polymer/polymer/lib/utils/async.js';
import {dom} from '@polymer/polymer/lib/legacy/polymer.dom';

import "@polymer/iron-ajax/iron-ajax";
import "@polymer/iron-list/iron-list";
import "@polymer/iron-scroll-threshold/iron-scroll-threshold";
import "@polymer/paper-spinner/paper-spinner";


import {mcsharedStyles} from "../moslemcorner-shared-styles/moslemcorner-shared-styles";
import {sharedStyles} from '../shared-styles/shared-styles';


import "./moslemcorner-data-grid-row";
import "./moslemcorner-data-grid-cell";


class MCDatagrid extends  mixinBehaviors([], PolymerElement) {
    static get template(){
        return html`
        ${mcsharedStyles}
        <style >
            :host {
                display: block;
            }
            iron-list {
                height: var(--moslemcorner-data-grid-height, 80vh); /* don't use % values unless the parent element is sized. */
            }
            nebula-loader {
                --nebula-loader-dialog-background-color: #2E3032;
                --nebula-loader-dialog-border-color: #FFFFFF;
                --nebula-loader-dialog-color: #ff4081;
            }
            .header {
                padding: var(--moslemcorner-data-grid-header-padding, 10px);
                color: var(--moslemcorner-data-grid-header-color, #000000);
                font-weight: var(--moslemcorner-data-grid-header-font-weight, bold);
                border-radius: 0;
                background-color: var(--moslemcorner-data-grid-header-background-color, #dddddd);
                border-bottom: solid;
                border-color: var(--moslemcorner-data-grid-header-border-color, #111111);
                border-bottom-width: thin;
            }
            .item {
                padding: var(--moslemcorner-data-grid-item-padding, 10px);
                color: var(--moslemcorner-data-grid-item-color, #757575);
                font-weight: var(--moslemcorner-data-grid-header-font-weight, normal);
                border-radius: 0;
                background-color: var(--moslemcorner-data-grid-item-background-color, #f9f9f9);
                border-bottom: solid;
                border-color: var(--moslemcorner-data-grid-item-border-color, #dddddd);
                border-bottom-width: thin;
            }
            .loader {
                margin-top: 2px;
                color: var(--moslemcorner-data-grid-loader-color, #FFFFFF);
                background-color: var(--moslemcorner-data-grid-loader-background-color, #1e88e5);
                opacity: 0.5;
                text-align: center;
                height: 30px;
                font: 13px arial;
                font-weight: bold;
                line-height: 30px;
            }
            .title iron-icon {
                padding: 0;
                padding-right: 2px;
            }
        </style>
        <iron-ajax
                id="dataAjax"
                url="[[dataUrl]]"
                handle-as="json"
                loading='{{_loading}}'
                on-response='_onResponse'
                on-error='_onError'>
        </iron-ajax>
        <iron-ajax
                id="deleteAjax"
                handle-as="json"
                loading='{{_loading}}'
                on-response='_onDeleteResponse'
                on-error='_onDeleteError'>
        </iron-ajax>
        <div class="card">
            <div class="header">
                <moslemcorner-data-grid-row>
                    <template is="dom-repeat" items="[[columns]]" as="column">
                        <moslemcorner-data-grid-cell
                                header
                                column="[[column]]"
                                template="[[column.headerTemplate]]"
                                width="[[column.width]]"
                                align="[[column.align]]">
                        </moslemcorner-data-grid-cell>
                    </template>
                </moslemcorner-data-grid-row>
            </div>
            <iron-scroll-threshold lower-threshold="{{threshold}}" on-lower-threshold="_loadMoreData" scroll-target="list" id="threshold">
                <iron-list items="[[data]]" as="item" id="list">
                    <template>
                        <div class="item">
                            <moslemcorner-data-grid-row>
                                <template is="dom-repeat" items="[[columns]]" as="column">
                                    <moslemcorner-data-grid-cell
                                            item="[[item]]"
                                            column="[[column]]"
                                            template="[[column.template]]"
                                            width="[[column.width]]">
                                    </moslemcorner-data-grid-cell>
                                </template>
                            </moslemcorner-data-grid-row>
                        </div>
                    </template>
                </iron-list>
                <div class="loader" hidden$="[[!_loading]]">{{loadingText}}</div>
            </iron-scroll-threshold>
            <content select="paper-fab"></content>
        </div>
        <paper-dialog class="card" id="error" modal>
            <div class="title"><iron-icon icon="icons:info"></iron-icon>[[_error]]</div>
            <div class="buttons">
                <paper-button dialog-confirm autofocus>Close</paper-button>
            </div>
        </paper-dialog>
        <!--nebula-loader id="loader" icon="icons:refresh" text="Please wait..."></nebula-loader-->
        <paper-spinner id="loader" indeterminate class="blue"></paper-spinner>
        <content select="moslemcorner-data-grid-column"></content>
    `;
    }
    static get properties(){
        return {
                    dataUrl: {
                        type:String,
                        value:''
                    },
                    page: {
                        type: Number,
                        value: 0
                    },
                    data: {
                        type: Array,
                        value: function() {
                            return [];
                        }
                    },
                    /**
                     * An array of `data-grid-column` elements which contain the templates
                     * to be stamped with items.
                     */
                    columns: {
                        type: Array,
                        notify: true,
                        value: function() {
                            return [];
                        },
                        observer: '_columnsChanged'
                    },
                    threshold: {
                        type: String,
                        value: 200
                    },
                    _error: {
                        type: String,
                        value: ''
                    },
                    _loading: {
                        type: Boolean,
                        value: false
                    },
                    _reload: {
                        type: Boolean,
                        value: false
                    },
                    loadingText: {
                        type: String,
                        value: "loading more data..."
                    }
                }
    }
    created() {
        this._observer = dom(this).observeNodes(function(info) {
            console.log(info,this);
            var hasColumns = function(node) {
                return (node.nodeType === Node.ELEMENT_NODE && node.tagName.toUpperCase() === 'MOSLEMCORNER-DATA-GRID-COLUMN');
            };

            if (info.addedNodes.filter(hasColumns).length > 0 || info.removedNodes.filter(hasColumns).length > 0) {
                this.set('columns', this.getContentChildren('[select=moslemcorner-data-grid-column]'));
                //this.notifyResize();
            }
        }.bind(this));
    }
    _columnsChanged(columns, oldColumns) {
        // NOP
    }
    reload() {
        console.log(info,this);
        super.reload();
        self  = this;
        async.microTask.run(function() {
            self._reload = true;
            self.page = 1;
            self.$.dataAjax.url = self.dataUrl + '?page=' + self.page.toString();
            self.$.dataAjax.generateRequest();
        });
    }
    _loadMoreData() {
        self = this;
        async.microTask.run(function() {
            self._reload = false;
            self.page++;
            self.$.dataAjax.url = self.dataUrl + '?page=' + self.page.toString();
            self.$.dataAjax.generateRequest();
        });
    }
    _onResponse(e) {
        console.log(e.detail.response,this);
        if (this._reload) {
            this.set('data', []);
            this._reload = false;
        } // clear data

        var response = e.detail.response;
        response.results.forEach(function(item) {
            this.push('data', item);
        }, this);

        // Clear the lower threshold so we can load more data when the user scrolls down again.
        this.$.threshold.clearTriggers();

        //this.$.loader.close();
    }
    _onError() {
        //this.$.loader.close();
        this._error = 'Something wrong... Please try again.';
        this.$.error.open();
    }
    _onDeleteResponse(e) {
        var response = e.detail.response;
        if (response.status == '200') {
//                        /* network delay simulation */
//                        self = this;
//                        setTimeout(function() {
//                            self.reload();
//                        }, 1500);
            this.reload();
        }
        else {
            //this.$.loader.close();
            this._error = 'Something wrong... Please try again.';
            this.$.error.open();
        }
    }
    _onDeleteError() {
        //this.$.loader.close();
        this._error = 'Something wrong... Please try again.';
        this.$.error.open();
    }
    delete(url) {
        self = this;
        async.microTask.run(function() {
            self.$.deleteAjax.url = url;
            self.$.deleteAjax.generateRequest();
            //self.$.loader.show();
        });
    }
}
customElements.define('moslemcorner-data-grid', MCDatagrid);
