import { PolymerElement, html } from '@polymer/polymer';

import '@polymer/paper-listbox/paper-listbox';
import '@polymer/iron-location/iron-location';

import {sharedStyles} from "../shared-styles/shared-styles"; 

class DrawerMenu extends PolymerElement {
    static get properties(){
        return {
            sections: {
                type: Array,
                value:['']
            }
        }
    }
    static get template(){
        return html`
        ${sharedStyles}
        <style>
        #drawerTitleContainer {
            width: 100%;
            height: 64px;
            display: table;
            background-color: #f3f3f3;
            border-bottom: 1px solid #e0e0e0;
        }
        #drawerTitle {
            display: table-cell;
            vertical-align: middle;
            text-align: center;
            font-weight: bold;
        }
        paper-menu, paper-item {
            display: block;
            padding: 5px;
            font-size: 15px;
        }
        </style>        
        <iron-location id="location" path="{{path}}" hash="{{hash}}" query="{{query}}" dwell-time="{{dwellTime}}"></iron-location>
        <div id="drawerTitleContainer"><div id="drawerTitle">markazunaa</div></div>
        <paper-menu tabindex="-1" multi>
            <paper-submenu id="contentSubmenu">
                <paper-item class="menu-trigger">Content</paper-item>
                <paper-menu class="menu-content sublist" id="contentItems">
                    <paper-item on-tap="_openUrl" id="articles">Articles</paper-item>
                    <paper-item on-tap="_openUrl" id="tags">Tags</paper-item>
                </paper-menu>
            </paper-submenu>
            <paper-submenu id="accessSubmenu">
                <paper-item class="menu-trigger">Access</paper-item>
                <paper-menu class="menu-content sublist"  id="accessItems">
                    <paper-item on-tap="_openUrl" id="groups">Groups</paper-item>
                    <paper-item on-tap="_openUrl" id="users">Users</paper-item>
                </paper-menu>
            </paper-submenu>
        </paper-menu>

        `; 
    }
    _openUrl(e){
        //this._removeFocus(e.target.id);
        var pageUrl =  '/admin/page/' + e.target.id;
        this.$.location.path = pageUrl
        window.location.reload(true);
    }
    _removeFocus(currentSelectedElementId) {
        // remove current selected paper-item
        if (currentSelectedElementId == 'groups' || currentSelectedElementId == 'users') {            
            this.$.contentItems.selectIndex(-1);
        }
        else {
            this.$.accessItems.selectIndex(-1);
        }
    }

}
customElements.define('drawer-menu', DrawerMenu);
